#pragma once

#include "AcubeSATParameters.hpp"
#include "Services/HousekeepingService.hpp"

using namespace AcubeSATParameters;

namespace HousekeepingStructures {
   static etl::array<HousekeepingStructure, ECSSMaxHousekeepingStructures> structures = {};
}