#pragma once

#include "Helpers/Parameter.hpp"
#include "SU_Definitions.hpp"
#include "AcubeSATParameters.hpp"

namespace CommonParameters {
    inline auto &boardTemperature1 = AcubeSATParameters::suPCBTemperature1;
    inline auto &mcuTemperature = AcubeSATParameters::suMCUTemperature;
    inline auto &time = AcubeSATParameters::suOnBoardTime;
    inline auto &useRTT = AcubeSATParameters::suUseRTT;
    inline auto &useUART = AcubeSATParameters::suUseUART;
    inline auto &useCAN = AcubeSATParameters::suUseCAN;
}

