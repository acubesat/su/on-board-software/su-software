#pragma once

#include "HAL_PWM.hpp"
#include "etl/array.h"
#include "LCLDACC.hpp"
#include "LCLPWM.hpp"

namespace LCLDefinitions {
    enum ProtectedDevicesDACC : uint8_t {
        CAN = 0,
        NAND = 1
    };
    enum ProtectedDevicesPWM : uint8_t {
        CAMERA = 0
    };

    static constexpr uint8_t NumberOfProtectedDevicesDACC = 2;

    inline static etl::array<LCLDACC, NumberOfProtectedDevicesDACC> lclDACArray = {
            LCLDACC(DACC_CHANNEL_0, PIO_PIN_PD21, PIO_PIN_PD20, LCLDACC::CAN),
            LCLDACC(DACC_CHANNEL_1, PIO_PIN_PD18, PIO_PIN_PD19, LCLDACC::NAND)

    };

    static constexpr uint8_t NumberOfProtectedDevicesPWM = 1;
    static constexpr uint8_t PeripheralNumber = 0;

    inline static etl::array<LCLPWM<PeripheralNumber>, NumberOfProtectedDevicesPWM> lclPWMArray = {
            LCLPWM<PeripheralNumber>(PWM_CHANNEL_0, PWM_CHANNEL_0_MASK, PIO_PIN_PD8, PIO_PIN_PD9,
                                     LCLPWM<PeripheralNumber>::CAMERA),
    };
}