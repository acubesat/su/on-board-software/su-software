#pragma once

#include "Task.hpp"
#include "U3VCamDriver.h"

/**
 * @class CamImgDataTask
 * @brief A FreeRTOS task class that handles the execution of the U3V camera driver tasks.
 */
class CamImgDataTask : public Task {
public:
    /** @brief Task stack depth */
    const static inline uint16_t TaskStackDepth = 1024;

    /** @brief Stack array for the task */
    StackType_t taskStack[TaskStackDepth];

    /**
     * @brief Constructor for CamImgDataTask.
     */
    CamImgDataTask() : Task("CamImgDataTask") {}

    /**
     * @brief The main execution function of the task.
     *
     * Initializes the U3V camera driver and enters a loop where it continuously
     * calls the driver tasks with a delay.
     */
    void execute();

    /**
     * @brief Creates the FreeRTOS task.
     *
     * Initializes the task with the specified name, priority, stack depth, and task buffer.
     */
    void createTask() {
        xTaskCreateStatic(vClassTask<CamImgDataTask>, this->TaskName, CamImgDataTask::TaskStackDepth,
                          this, tskIDLE_PRIORITY + 3, this->taskStack,
                          &(this->taskBuffer));
    }
};

/** @brief Optional instance of CamImgDataTask */
inline std::optional<CamImgDataTask> camImgDataTask;
