#pragma once

#include "Task.hpp"
#include "plib_pio.h"
#include "U3VCamDriver.h"
#include "plib_usart1.h"
#include "sys_dma.h"

/**
 * @class U3VCamDriverDataTask
 * @brief This class handles the task of interfacing with the U3V camera driver
 *        and USART, managing image data transmission and controlling state machines.
 */
class U3VCamDriverDataTask : public Task {
public:

    /**
     * @enum APP_STATES
     * @brief Defines the states used by the application's state machine.
     */
    typedef enum {
        /** Initial state of the application */
        APP_STATE_INIT = 0,
        /** State to service application tasks */
        APP_STATE_SERVICE_TASKS,
        /** TODO: Additional states can be defined here */
    } APP_STATES;

    /**
     * @struct APP_DATA
     * @brief Stores application-specific data, including state, driver handles,
     *        image request flags, payload counters, and image data buffer.
     */
    typedef struct {
        APP_STATES state;           /**< Current state of the application */
        DRV_HANDLE usrtDrv;         /**< Handle for the USART driver */
        bool imgRequested;          /**< Flag to check if an image is requested */
        bool imgPldPending;         /**< Flag to check if an image payload is pending */
        uint32_t payldSizeCnt;      /**< Counter for the payload size */
        uint8_t imgData[0x8000];    /**< Buffer to hold image data */
        /** TODO: Define any additional data used by the application */
    } APP_DATA;

    /** Static instance of APP_DATA struct to hold application data */
    static APP_DATA appData;

    /** Stack depth for the task */
    const static inline uint16_t TaskStackDepth = 1024;

    /** Task stack array */
    StackType_t taskStack[TaskStackDepth];

    /**
     * @brief Constructor for the U3VCamDriverDataTask class.
     */
    U3VCamDriverDataTask() : Task("U3VCamDriverDataTask") {}

    /**
     * @brief Initialize the application. This function sets the initial state of the
     *        application's state machine and prepares the application.
     */
    static void APP_Initialize(void);

    /**
     * @brief Main task function that handles the state machine and application logic.
     *        This function continuously checks and processes application states.
     */
    static void APP_Tasks(void);

    /**
     * @brief Callback function for receiving image payload blocks from the camera driver.
     * @param event The type of event related to the image acquisition.
     * @param imgData Pointer to the image data block.
     * @param blockSize Size of the data block.
     * @param blockCnt Number of the block being processed.
     */
    static void _APP_U3vImgPldBlkRcvdCbk(T_U3VCamDriverImageAcqPayloadEvent event, void *imgData, size_t blockSize, uint32_t blockCnt);

    /**
     * @brief Callback function for handling SW1 press interrupts.
     * @param pin The pin associated with the interrupt.
     * @param context User-defined context pointer.
     */
    static void _APP_PioSw1PrsdCbk(PIO_PIN pin, uintptr_t context); // SW1 interrupt handler

    /**
     * @brief Execute the main task of the U3VCamDriverDataTask. Initializes the application
     *        and then runs the task in a loop.
     */
    void execute();

    /**
     * @brief Creates the FreeRTOS task and assigns the appropriate parameters such as stack depth
     *        and priority.
     */
    void createTask() {
        xTaskCreateStatic(vClassTask < U3VCamDriverDataTask > , this->TaskName, U3VCamDriverDataTask::TaskStackDepth,
                          this, tskIDLE_PRIORITY + 3, this->taskStack,
                          &(this->taskBuffer));
    }
};

/** Optional instance of the U3VCamDriverDataTask class */
inline std::optional<U3VCamDriverDataTask> u3vCamDriverDataTask;