#pragma once

#include "Task.hpp"

/**
 * The first task to run after starting the scheduler used to spawn the rest of the tasks.
 */
class UsbHostTask: public Task {
public:
    const static inline uint16_t TaskStackDepth = 1024;

    StackType_t taskStack[TaskStackDepth];

    UsbHostTask() : Task("UsbHostTask") {}

    void execute();

    void createTask() {
        xTaskCreateStatic(vClassTask<UsbHostTask>, this->TaskName, UsbHostTask::TaskStackDepth,
                          this, tskIDLE_PRIORITY + 4, this->taskStack,
                          &(this->taskBuffer));
    }
};

inline std::optional<UsbHostTask> usbHostTask;
