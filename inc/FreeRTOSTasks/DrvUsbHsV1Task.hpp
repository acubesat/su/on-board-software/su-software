#pragma once

extern "C" {
#include "drv_usbhsv1.h"
}

#include "Task.hpp"

/**
 * The first task to run after starting the scheduler used to spawn the rest of the tasks.
 */
class DrvUsbHostTask: public Task {
public:
    const static inline uint16_t TaskStackDepth = 1024;

    StackType_t taskStack[TaskStackDepth];

    DrvUsbHostTask() : Task("DrvUsbHostTask") {}

   void execute();

    void createTask() {
        xTaskCreateStatic(vClassTask<DrvUsbHostTask>, this->TaskName, DrvUsbHostTask::TaskStackDepth,
                          this, tskIDLE_PRIORITY + 5, this->taskStack,
                          &(this->taskBuffer));
    }
};

inline std::optional<DrvUsbHostTask> drvUsbHostTask;