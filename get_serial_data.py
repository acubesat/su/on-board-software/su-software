from os import chmod, stat
from serial import Serial
from pathlib import Path


class SUSoftwareError(Exception):
    """Base error class for SU Software"""


class ProjectIndicatorNotFound(SUSoftwareError):
    """The project indicator file (e.g. `.here`) was not found in this or any parent directory"""


def find_root(path=Path.cwd(), project_indicator=".here"):
    """Find the root directory for the project based on the presence of a project indicator file"""
    if path.joinpath(project_indicator).exists():
        return path.resolve()
    elif (path.parent.resolve() != path.resolve()):
        return find_root(path.parent)
    else:
        raise ProjectIndicatorNotFound()


def here(*paths):
    """Resolve the absolute path for the given relative path components within the project root"""
    root = find_root()
    return root.joinpath(*paths).resolve()


def main():
    output_path = here("output/output_img.raw")
    output_path.parents[0].mkdir(parents=True, exist_ok=True)
    serial_device = '/dev/ttyACM0'  # ttyACM0 -> USB serial communication device (CDC) | In Windows it's COM*, e.g. COM1
    baudrate = 115200  # check your baud rate setting for MHC generated file > plib_usart1.c > line201: USART1_REGS->US_BRGR = US_BRGR_CD(81);
    timeout_sel = 3000  # set high to avoid re-writing a new file stream if timeout runs out

    with open(output_path, 'wb') as fl:
        original_permissions = stat(output_path).st_mode
        try:
            chmod(output_path, 0o777)
            with Serial(serial_device, baudrate, timeout=timeout_sel) as ser:
                while True:
                    message = ser.read()
                    # print(message)
                    fl.write(message)
        finally:
            chmod(output_path, original_permissions)


if __name__ == "__main__":
    main()