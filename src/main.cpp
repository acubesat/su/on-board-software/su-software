
#include "main.h"

#include "FreeRTOS.h"

#include "list.h"
#include "queue.h"
#include "task.h"
#include "U3VCamDriverDataTask.hpp"
#include "CamImgDataTestTask.hpp"
#include "UsbHostTask.hpp"
#include "DrvUsbHsV1Task.hpp"

#include "definitions.h"

#include "SU_Definitions.hpp"
#include "InitializationTask.hpp"

#define IDLE_TASK_SIZE 1000

#if configSUPPORT_STATIC_ALLOCATION
/* static memory allocation for the IDLE task */
static StaticTask_t xIdleTaskTCBBuffer;
static StackType_t xIdleStack[IDLE_TASK_SIZE];

extern "C" void vApplicationGetIdleTaskMemory(StaticTask_t **ppxIdleTaskTCBBuffer, StackType_t **ppxIdleTaskStackBuffer,
                                              uint32_t *pulIdleTaskStackSize) {
    *ppxIdleTaskTCBBuffer = &xIdleTaskTCBBuffer;
    *ppxIdleTaskStackBuffer = &xIdleStack[0];
    *pulIdleTaskStackSize = IDLE_TASK_SIZE;
}

#endif

//
extern "C" void main_cpp() {

    SYS_Initialize(NULL);
    initializationTask.emplace();
    usbHostTask.emplace();
    drvUsbHostTask.emplace();
    camImgDataTask.emplace();
    u3vCamDriverDataTask.emplace();


    initializationTask->createTask();
    usbHostTask->createTask();
    drvUsbHostTask->createTask();
    camImgDataTask->createTask();
    u3vCamDriverDataTask->createTask();


    vTaskStartScheduler();

    while (true) {
        /* Maintain state machines of all polled MPLAB Harmony modules. */
        SYS_Tasks();
    }

    return;
}
