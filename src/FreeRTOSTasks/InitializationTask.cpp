#include "InitializationTask.hpp"

void InitializationTask::execute() {
    initializeTasks();

    vTaskSuspend(nullptr);
}
