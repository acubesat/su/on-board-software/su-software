#include "DrvUsbHsV1Task.hpp"

extern "C" void DrvUsbHostTask::execute() {

    while(true){
        /* USB HS Driver Task Routine */
        DRV_USBHSV1_Tasks(sysObj.drvUSBHSV1Object);
        vTaskDelay(pdMS_TO_TICKS(10));
    }
}