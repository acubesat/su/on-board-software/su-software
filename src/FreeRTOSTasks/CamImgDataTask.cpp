#include "CamImgDataTestTask.hpp"

void CamImgDataTask::execute() {
    while (true) {
        /* USB Camera Driver Task Routine */
        U3VCamDriver_Tasks();
        vTaskDelay(pdMS_TO_TICKS(10));
    }
}