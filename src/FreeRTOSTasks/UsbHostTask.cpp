#include "UsbHostTask.hpp"

void UsbHostTask::execute() {
    while(true){
        /* USB Host layer Task Routine */
        USB_HOST_Tasks(sysObj.usbHostObject0);
        vTaskDelay(pdMS_TO_TICKS(10));
    }
}