#include "U3VCamDriverDataTask.hpp"

void U3VCamDriverDataTask::execute() {
    APP_Initialize();
    while (true) {
        APP_Tasks();
        vTaskDelay(pdMS_TO_TICKS(10));
    }
}


/**
 * @var U3VCamDriverDataTask::appData
 * @brief Static instance of APP_DATA which holds the application's current state, flags, and data.
 */
U3VCamDriverDataTask::APP_DATA U3VCamDriverDataTask::appData;


void U3VCamDriverDataTask::APP_Initialize(void) {
    /* Place the App state machine in its initial state. */
    appData.state = APP_STATE_INIT;
    appData.imgRequested = false;
    appData.imgPldPending = false;

    /* TODO: Initialize your application's state machine and other
     * parameters.
     */
}


void U3VCamDriverDataTask::APP_Tasks(void) {
    /* Check the application's current state. */
    switch (appData.state) {
        /* Application's initial state. */
        case APP_STATE_INIT: {
            if ((U3V_CAM_DRV_OK == U3VCamDriver_SetImagePayldTransfParams(_APP_U3vImgPldBlkRcvdCbk, appData.imgData)) &&
                (PIO_PinInterruptCallbackRegister(GPIO_PB12_PIN, _APP_PioSw1PrsdCbk, 0))) {
                appData.usrtDrv = DRV_USART_Open(0, DRV_IO_INTENT_WRITE);
                PIO_PinInterruptEnable(GPIO_PB12_PIN);
                appData.state = APP_STATE_SERVICE_TASKS;
            }
            break;
        }

        case APP_STATE_SERVICE_TASKS: {
            if (appData.imgRequested && !appData.imgPldPending) {
                if (!SYS_DMA_ChannelIsBusy(SYS_DMA_CHANNEL_1)) {
                    if (U3V_CAM_DRV_OK == U3VCamDriver_RequestNewImagePayloadBlock()) {
                        appData.imgPldPending = true;
                    }
                } else {
                    appData.state = APP_STATE_SERVICE_TASKS; // debug
                }
            }
            break;
        }

            /* TODO: implement your application state machine.*/


            /* The default state should never be executed. */
        default: {
            /* TODO: Handle error in application's state machine. */
            break;
        }
    }
}


void U3VCamDriverDataTask::_APP_U3vImgPldBlkRcvdCbk(T_U3VCamDriverImageAcqPayloadEvent event, void *imgData,
                                                    size_t blockSize, uint32_t blockCnt) {
    void *srcAddr = imgData;
    size_t size = blockSize;

    switch (event) {
        case U3V_CAM_DRV_IMG_LEADER_DATA:
            appData.payldSizeCnt = 0U;
            break;

        case U3V_CAM_DRV_IMG_TRAILER_DATA:
            appData.imgRequested = false;
            break;

        case U3V_CAM_DRV_IMG_PAYLOAD_DATA:
            if (true == DRV_USART_WriteBuffer(appData.usrtDrv, srcAddr, size)) {
                /* Tx ok */
            } else {
                size = blockSize; // error breakpoint position, should never pass from here
            }
            appData.payldSizeCnt += size;
            break;

        default:
            break;
    }
    appData.imgPldPending = false;
}


void U3VCamDriverDataTask::_APP_PioSw1PrsdCbk(PIO_PIN pin, uintptr_t context) // SW1 interrupt handler
{
    (void) pin;
    (void) context;
    appData.imgRequested = true;
}