#include "ECSS_Configuration.hpp"

#ifdef SERVICE_FUNCTION
#include "Platform/FunctionManagementServiceTestFunctions.hpp"
#include "ServicePool.hpp"

void FunctionManagementService::initializeFunctionMap() {
    auto &functionManagement = Services.functionManagement;
    functionManagement.include(String<ECSSFunctionNameLength>("testCustomLogFunction"), &testCustomLogFunction);
    functionManagement.include(String<ECSSFunctionNameLength>("getPicture"),reinterpret_cast<void (*)(String<ECSSFunctionMaxArgLength>)>(&getPicture));
}

#endif